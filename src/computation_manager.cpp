// SPDX-License-Identifier: GPL-3.0-only
#include "cpp-util/computation_manager.hpp"

namespace jk::cpputil::computation_manager {
void cancellable_computation_from_function::run() { func(ssource.get_token()); }

void cancellable_computation_from_function::cancel() { ssource.request_stop(); }
} // namespace game_tree_genetator::computation_manager
