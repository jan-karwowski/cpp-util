#include <boost/test/unit_test.hpp>

#include <barrier>

#include "cpp-util/computation_manager.hpp"

using namespace jk::cpputil::computation_manager;

BOOST_AUTO_TEST_SUITE(computation_manager_test)

BOOST_AUTO_TEST_CASE(manager_without_taksk_can_be_stopped) {
  { computation_manager<std::string> manager(2); }
  BOOST_TEST(true);
}

BOOST_AUTO_TEST_CASE(task_that_are_not_started_are_just_removed) {
  std::barrier barrier(3);
  bool flag1 = false;
  bool flag2 = false;
  bool flag3 = false;

  std::unique_ptr<computation> c1(
      new cancellable_computation_from_function([&flag1, &barrier](auto) {
        barrier.arrive_and_wait();
        flag1 = true;
      }));
  std::unique_ptr<computation> c2(
      new cancellable_computation_from_function([&flag2, &barrier](auto) {
        barrier.arrive_and_wait();
        flag2 = true;
      }));
  std::unique_ptr<computation> c3(new cancellable_computation_from_function(

      [&flag3](auto) {
        std::cout << "task3 start" << std::endl;
        ;
        flag3 = true;
      }));

  {
    computation_manager<std::string> manager(2);

    auto id1 = manager.schedule_computation_for_category("1", std::move(c1));
    auto id2 = manager.schedule_computation_for_category("1", std::move(c2));
    auto id3 = manager.schedule_computation_for_category("1", std::move(c3));
    BOOST_REQUIRE_LT(id1, id2);
    BOOST_REQUIRE_LT(id2, id3);

    {
      bool ret = manager.cancel_computation(id3);
      BOOST_REQUIRE(ret);
    }
    barrier.arrive_and_wait();
  }

  BOOST_REQUIRE(flag1);
  BOOST_REQUIRE(flag2);
  BOOST_REQUIRE(!flag3);
}

BOOST_AUTO_TEST_CASE(one_package_can_be_canceled_before_running) {
  std::barrier barrier(2);
  bool flag1 = false;
  bool flag2 = false;
  bool flag3 = false;
  bool flag4 = false;

  std::unique_ptr<computation> c1(
      new cancellable_computation_from_function([&flag1, &barrier](auto) {
        barrier.arrive_and_wait();
        flag1 = true;
      }));
  std::unique_ptr<computation> c2(new cancellable_computation_from_function(
      [&flag2](auto) { flag2 = true; }));
  std::unique_ptr<computation> c3(new cancellable_computation_from_function(

      [&flag3, &barrier](auto) {
        flag3 = true;
        barrier.arrive_and_wait();
      }));
  std::unique_ptr<computation> c4(new cancellable_computation_from_function(

      [&flag4](auto) { flag4 = true; }));

  {
    computation_manager<std::string> manager(1);

    auto id1 = manager.schedule_computation_for_category("1", std::move(c1));
    auto id2 = manager.schedule_computation_for_category("2", std::move(c2));
    auto id3 = manager.schedule_computation_for_category("1", std::move(c3));
    auto id4 = manager.schedule_computation_for_category("2", std::move(c4));
    BOOST_REQUIRE_LT(id1, id2);
    BOOST_REQUIRE_LT(id2, id3);
    BOOST_REQUIRE_LT(id3, id4);
    { manager.cancel_all_computations_for_category("2"); }
    barrier.arrive_and_wait();
    barrier.arrive_and_wait();
  }

  BOOST_REQUIRE(flag1);
  BOOST_REQUIRE(!flag2);
  BOOST_REQUIRE(flag3);
  BOOST_REQUIRE(!flag4);
}

BOOST_AUTO_TEST_CASE(tasks_are_run_in_order) {
  std::barrier barrier(2);
  std::vector<int> order;
  const int count = 10;

  std::vector<std::unique_ptr<computation>> computations;

  for (int i = 0; i < count; i++) {
    computations.push_back(
        std::unique_ptr<computation>(new cancellable_computation_from_function(
            [&order, i](auto) { order.push_back(i); })));
  }
  computations.push_back(
      std::unique_ptr<computation>(new cancellable_computation_from_function(
          [&order, count, &barrier](auto) {
            order.push_back(count);
            barrier.arrive_and_wait();
          })));

  {
    computation_manager<std::string> manager(1);
    for (auto &comp : computations) {
      manager.schedule_computation_for_category("1", std::move(comp));
    }
    barrier.arrive_and_wait();
  }

  std::vector<int> num{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

  BOOST_REQUIRE_EQUAL_COLLECTIONS(order.begin(), order.end(), num.begin(),
                                  num.end());
}
BOOST_AUTO_TEST_SUITE_END()
