// SPDX-License-Identifier: GPL-3.0-only

#include <boost/test/unit_test.hpp>

#include <string>

#include "cpp-util/lru.hpp"

using namespace jk::cpputil::lru;

BOOST_AUTO_TEST_SUITE(order_list)

BOOST_AUTO_TEST_CASE(puting_elements_in_proper_order) {
  lru_order_list<int> list;

  list.push_most_recently_used(2);
  list.push_most_recently_used(5);
  list.push_most_recently_used(7);
  list.push_most_recently_used(1);

  std::vector<int> expected{1, 7, 5, 2};
  BOOST_REQUIRE_EQUAL(list.size(), 4);
  BOOST_REQUIRE_EQUAL_COLLECTIONS(list.begin(), list.end(), expected.begin(),
                                  expected.end());
}

BOOST_AUTO_TEST_CASE(empty_list_has_size_zero) {
  lru_order_list<int> list;
  BOOST_REQUIRE_EQUAL(list.size(), 0);
  BOOST_REQUIRE_EQUAL(list.begin(), list.end());
}

BOOST_AUTO_TEST_CASE(elements_are_removed_in_least_recently_used_order) {
  lru_order_list<int> list;

  list.push_most_recently_used(2);
  list.push_most_recently_used(5);
  list.push_most_recently_used(7);
  list.push_most_recently_used(1);
  BOOST_REQUIRE_EQUAL(list.size(), 4);

  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(el.has_value());
    BOOST_REQUIRE_EQUAL(2, el.value());
  }
  BOOST_REQUIRE_EQUAL(list.size(), 3);
  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(el.has_value());
    BOOST_REQUIRE_EQUAL(5, el.value());
  }
  BOOST_REQUIRE_EQUAL(list.size(), 2);
  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(el.has_value());
    BOOST_REQUIRE_EQUAL(7, el.value());
  }
  BOOST_REQUIRE_EQUAL(list.size(), 1);
  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(el.has_value());
    BOOST_REQUIRE_EQUAL(1, el.value());
  }
  BOOST_REQUIRE_EQUAL(list.size(), 0);
  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(!el.has_value());
  }
}

BOOST_AUTO_TEST_CASE(move_to_front) {
  lru_order_list<int> list;

  list.push_most_recently_used(2);
  auto nn = list.push_most_recently_used(5);
  list.push_most_recently_used(7);

  list.mark_most_recently_used(nn);
  std::vector<int> expected{5, 7, 2};
  BOOST_REQUIRE_EQUAL_COLLECTIONS(list.begin(), list.end(), expected.begin(),
                                  expected.end());

  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(el.has_value());
    BOOST_REQUIRE_EQUAL(2, el.value());
  }
  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(el.has_value());
    BOOST_REQUIRE_EQUAL(7, el.value());
  }
  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(el.has_value());
    BOOST_REQUIRE_EQUAL(5, el.value());
  }
  {
    auto el = list.pop_least_recently_used();
    BOOST_REQUIRE(!el.has_value());
  }
}

BOOST_AUTO_TEST_SUITE_END()
