// SPDX-License-Identifier: GPL-3.0-only

#define BOOST_TEST_MODULE lru_cache_test
#include <boost/test/unit_test.hpp>

#include <string>

#include "cpp-util/lru.hpp"

using namespace jk::cpputil::lru;

struct tester {
  int val;
  tester(int val) : val(val) {}
  tester(const tester &t) : val(t.val) {}
  tester(tester &&t) : val(t.val) { t.val = -1; }

  tester &operator=(const tester &t) {
    this->val = t.val;
    return *this;
  }

  tester &operator=(tester &&t) {
    this->val = t.val;
    t.val = -1;
    return *this;
  }
};

BOOST_AUTO_TEST_SUITE(lru_cache_test)

BOOST_AUTO_TEST_CASE(can_insert_and_retreive_single_element_from_cache) {
  lru<int, std::string> cache(2);

  std::pair<bool, const std::string &> ret = cache.put(1, "1");
  BOOST_REQUIRE_EQUAL(ret.first, true);
  BOOST_REQUIRE_EQUAL("1", ret.second);

  std::optional<std::reference_wrapper<const std::string>> val =
      cache.lookup(1);
  BOOST_REQUIRE(val.has_value());
  BOOST_REQUIRE_EQUAL(val.value().get(), "1");
}

BOOST_AUTO_TEST_CASE(the_first_inserted_element_is_removed_when_adding_new) {
  lru<int, std::string> cache(3);

  {
    std::pair<bool, const std::string &> ret = cache.put(1, "1");
    BOOST_REQUIRE_EQUAL(ret.first, true);
    BOOST_REQUIRE_EQUAL("1", ret.second);
  }

  {
    std::pair<bool, const std::string &> ret = cache.put(2, "2");
    BOOST_REQUIRE_EQUAL(ret.first, true);
    BOOST_REQUIRE_EQUAL("2", ret.second);
  }

  {
    std::pair<bool, const std::string &> ret = cache.put(3, "3");
    BOOST_REQUIRE_EQUAL(ret.first, true);
    BOOST_REQUIRE_EQUAL("3", ret.second);
  }

  {
    std::pair<bool, const std::string &> ret = cache.put(4, "4");
    BOOST_REQUIRE_EQUAL(ret.first, true);
    BOOST_REQUIRE_EQUAL("4", ret.second);
  }

  BOOST_REQUIRE(!cache.lookup(1).has_value());

  {
    auto ret = cache.lookup(2);
    BOOST_REQUIRE(ret.has_value());
    BOOST_REQUIRE_EQUAL(ret.value().get(), "2");
  }
  {
    auto ret = cache.lookup(3);
    BOOST_REQUIRE(ret.has_value());
    BOOST_REQUIRE_EQUAL(ret.value().get(), "3");
  }
  {
    auto ret = cache.lookup(4);
    BOOST_REQUIRE(ret.has_value());
    BOOST_REQUIRE_EQUAL(ret.value().get(), "4");
  }
}

BOOST_AUTO_TEST_CASE(adding_the_same_element_twice_does_not_overflow_cache) {
  lru<int, std::string> cache(3);

  {
    std::pair<bool, const std::string &> ret = cache.put(1, "1");
    BOOST_REQUIRE_EQUAL(ret.first, true);
    BOOST_REQUIRE_EQUAL("1", ret.second);
  }

  {
    std::pair<bool, const std::string &> ret = cache.put(2, "2");
    BOOST_REQUIRE_EQUAL(ret.first, true);
    BOOST_REQUIRE_EQUAL("2", ret.second);
  }

  {
    std::pair<bool, const std::string &> ret = cache.put(3, "3");
    BOOST_REQUIRE_EQUAL(ret.first, true);
    BOOST_REQUIRE_EQUAL("3", ret.second);
  }

  {
    std::pair<bool, const std::string &> ret = cache.put(3, "3");
    BOOST_REQUIRE_EQUAL(ret.first, false);
    BOOST_REQUIRE_EQUAL("3", ret.second);
  }

  {
    auto ret = cache.lookup(1);
    BOOST_REQUIRE(ret.has_value());
    BOOST_REQUIRE_EQUAL(ret.value().get(), "1");
  }
  {
    auto ret = cache.lookup(2);
    BOOST_REQUIRE(ret.has_value());
    BOOST_REQUIRE_EQUAL(ret.value().get(), "2");
  }
  {
    auto ret = cache.lookup(3);
    BOOST_REQUIRE(ret.has_value());
    BOOST_REQUIRE_EQUAL(ret.value().get(), "3");
  }
}

BOOST_AUTO_TEST_CASE(lookup_changes_the_least_recently_used_order) {
  lru<int, std::string> cache(10);

  for (int i = 0; i < 10; i++) {
    cache.put(i, std::to_string(i));
  }

  cache.lookup(3);
  cache.put(10, "10");

  for (int i = 0; i <= 10; i++) {
    auto res = cache.lookup(i);
    if (i == 0) {
      BOOST_REQUIRE(!res.has_value());
    } else {
      BOOST_REQUIRE(res.has_value());
      BOOST_REQUIRE_EQUAL(res.value().get(), std::to_string(i));
    }
  }
}

BOOST_AUTO_TEST_CASE(put_for_the_same_key_replaces_the_value) {
  lru<int, std::string> cache(10);
  cache.put(1, "aa");
  cache.put(1, "bb");
  auto res = cache.lookup(1);
  BOOST_REQUIRE(res.has_value());
  BOOST_REQUIRE_EQUAL(res.value().get(), "bb");
}

BOOST_AUTO_TEST_CASE(put_changes_the_least_recently_used_order) {
  lru<int, std::string> cache(10);

  for (int i = 0; i < 10; i++) {
    cache.put(i, std::to_string(i));
  }

  cache.put(3, "3");
  cache.put(10, "10");

  for (int i = 0; i <= 10; i++) {
    auto res = cache.lookup(i);
    if (i == 0) {
      BOOST_REQUIRE(!res.has_value());
    } else {
      BOOST_REQUIRE(res.has_value());
      BOOST_REQUIRE_EQUAL(res.value().get(), std::to_string(i));
    }
  }
}

BOOST_AUTO_TEST_CASE(put_by_copy_works) {
  const tester t(1);
  lru<int, tester> cache(10);
  auto res = cache.put(1, t);
  BOOST_REQUIRE_EQUAL(t.val, 1);
  BOOST_REQUIRE_EQUAL(res.second.val, 1);
  auto res2 = cache.lookup(1);
  BOOST_REQUIRE(res2.has_value());
  BOOST_REQUIRE_EQUAL(res2.value().get().val, 1);
  BOOST_REQUIRE_EQUAL(&(res2.value().get()), &(res.second));
}

BOOST_AUTO_TEST_CASE(put_by_move_works) {
  tester t(1);
  lru<int, tester> cache(10);
  auto res = cache.put(1, std::move(t));
  BOOST_REQUIRE_EQUAL(t.val, -1);
  BOOST_REQUIRE_EQUAL(res.second.val, 1);
  auto res2 = cache.lookup(1);
  BOOST_REQUIRE(res2.has_value());
  BOOST_REQUIRE_EQUAL(res2.value().get().val, 1);
  BOOST_REQUIRE_EQUAL(&(res2.value().get()), &(res.second));
}

BOOST_AUTO_TEST_CASE(put_by_function_works) {
  lru<int, std::string> cache(10);
  int counter = 0;
  auto ret = cache.lookup_or_put(1, [&counter]() {
    counter++;
    return "aaa";
  });
  BOOST_REQUIRE_EQUAL(counter, 1);
  BOOST_REQUIRE(ret.first);
  BOOST_REQUIRE_EQUAL(ret.second, "aaa");
  auto ret2 = cache.lookup(1);
  BOOST_REQUIRE(ret2.has_value());
  const std::string& ret2r = ret2.value();
  BOOST_REQUIRE_EQUAL(ret2r, "aaa");
}

BOOST_AUTO_TEST_CASE(put_by_function_noc_called_if_exists) {
  lru<int, std::string> cache(10);

  cache.put(1, "bbb");

  int counter = 0;
  auto ret = cache.lookup_or_put(1, [&counter]() {
    counter++;
    return "aaa";
  });
  BOOST_REQUIRE_EQUAL(counter, 0);
  BOOST_REQUIRE(!ret.first);
  BOOST_REQUIRE_EQUAL(ret.second, "bbb");
  auto ret2 = cache.lookup(1);
  BOOST_REQUIRE(ret2.has_value());
  BOOST_REQUIRE_EQUAL(ret2.value().get(), "bbb");
}

BOOST_AUTO_TEST_SUITE_END()
