# cpp-util

Various utility classes in c++-20

## Usage exmaple

Configure to use with cmake project: 

```CMake
FetchContent_Declare(cpp-util
  GIT_REPOSITORY https://gitlab.com/jan-karwowski/cpp-util.git
  GIT_TAG 8774f14638a297313c6116b20f971a07afb2420e
  )
FetchContent_MakeAvailable(cpp-util)
```

## Contents

See doxygen for details about each module.

### lru-cache

A cache that can store limited number of records in memory. If the record count is exceeded the least recently used element is removed from the cache. 

### computation_manager

A simple fixed thread pool with task queue and ability to cancel queued tasks.

### Boost Hana string helpers

Helper templates to convert numbers into Boost Hana strings

