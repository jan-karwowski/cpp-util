#ifndef BASIC_CONCEPTS_H
#define BASIC_CONCEPTS_H

#include <concepts>
#include <functional>

namespace jk::cpputil {

/** \brief Requirements for a type to be used with hash maps and hash sets */
template <class T>
concept Hashable =
    std::equality_comparable<T> && requires(const T &t) {
                                     {
                                       std::hash<T>{}(t)
                                       } -> std::convertible_to<int>;
                                   };

}

#endif /* BASIC_CONCEPTS_H */
