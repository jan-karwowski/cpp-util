// SPDX-License-Identifier: GPL-3.0-only
#ifndef LRU_H
#define LRU_H

#include <memory>
#include <optional>
#include <unordered_map>

namespace jk::cpputil::lru {

template <class Fun, class V>
concept generator = requires(Fun &&fun) {
                      { fun() } -> std::convertible_to<V>;
                    };

/** \brief A class that implements a doubly linked list with move to front
   operation (called \ref mark_most_recently_used).

    The class is used to store the order of how recently keys in \ref lru were
   used. Internally is used doubly linked list. The first element of the list is
   the most recently used one and the last is the least recently used one.

   The structure does not offer a copy constructor.

    Tehcnical details: the ownership of the element is managed by unique
   pointers. The list owns the first element, the first element owns the second
   one, etc.

    The class partially implements stl-compatible iterators. The full support is
   planned in the future.
*/
template <class Key> class lru_order_list {
  struct cache_order {
    std::unique_ptr<cache_order> next;
    cache_order *prev;
    Key key;
  };

  std::unique_ptr<cache_order>
      most_recently_used;           ///< the first element of the list
  cache_order *least_recently_used; ///< the last element of the list
  size_t node_count; ///< the element counter to quickly get the number of
                     ///< elements. Useful for testing.

public:
  /** The structure used to request marking the node as the most recently used.

   See \ref mark_most_recently_used(node_marker node)
  */
  struct node_marker {
    cache_order *node;
  };

  /** The iterator over the list */
  class iterator {
    cache_order *node;

  public:
    iterator(cache_order *node) : node(node) {}
    iterator &operator++() {
      node = node->next.get();
      return *this;
    }

    Key &operator*() { return node->key; }
    Key *operator->() { return &(node->key); }
    friend bool operator==(const iterator &, const iterator &) = default;
    friend std::ostream &operator<<(std::ostream &os, const iterator &it) {
      os << "iterator=" << it.node;
      return os;
    }
  };

  /** Initializes an empty list */
  lru_order_list()
      : most_recently_used(nullptr), least_recently_used(nullptr),
        node_count(0) {}

  /** Takes the least recently used element from the list (the last one) removes
     it and returns the key.

      Returns an empty optional if the list is empty. Time complexity O(1).
  */
  std::optional<Key> pop_least_recently_used() {
    if (least_recently_used == nullptr) {
      return std::optional<Key>();
    } else {
      std::unique_ptr<cache_order> to_return =
          least_recently_used->prev == nullptr
              ? std::move(most_recently_used)
              : std::move(least_recently_used->prev->next);
      least_recently_used = to_return->prev;
      --node_count;
      return to_return->key;
    }
  }

  /** Returns the number of elements in the list. */
  size_t size() { return node_count; }

  /** Makes the element marked by \ref node_marker as the most_recently used.

      \param node The element is moved to the front of the list. The node
     must come from earlier call to \ref push_most_recently_used on that
     particular list.
   */
  void mark_most_recently_used(node_marker node) {
    if (most_recently_used.get() == nullptr) {
      throw std::invalid_argument("the list is empty");
    }
    if (node.node == nullptr) {
      throw new std::invalid_argument("the node marker points to nullptr");
    }
    if (most_recently_used.get() == node.node) {
      return;
    } else {
      auto oldprev = node.node->prev; // not null because we are in else
      std::unique_ptr<cache_order> the_node(std::move(oldprev->next));
      oldprev->next = std::move(the_node->next);
      if (oldprev->next.get() != nullptr) {
        oldprev->next->prev = oldprev;
      }
      most_recently_used->prev = the_node.get();
      the_node->next = std::move(most_recently_used);
      the_node->prev = nullptr;
      most_recently_used = std::move(the_node);
    }
  }

  /** Add a new element as the most recently used (inserts as the first
     element).

      \return A \ref node_marker that can be used by \ref
     mark_most_recently_used to move element to front.
   */
  node_marker push_most_recently_used(const Key &key) {
    ++node_count;
    std::unique_ptr<cache_order> to_add(
        new cache_order{std::move(most_recently_used), nullptr, key});

    if (to_add->next == nullptr) {
      least_recently_used = to_add.get();
    } else {
      to_add->next->prev = to_add.get();
    }
    most_recently_used = std::move(to_add);

    return node_marker{most_recently_used.get()};
  }
  iterator begin() { return iterator(most_recently_used.get()); }
  iterator end() { return iterator(nullptr); }
};

/** \brief Least recently used (LRU) cache.
 *
 * \tparam Key the keys for the values stored in cache. Must fulfill
 * requirements for keys in std::unordered_map
 * \tparam Value the values for the cache
 *
 *  The cache stores the number of instances given as the constructor
 * parameters. If the caller wants to add the next element to the cache and the
 * cache currently holds the maximum numnber of elements allowed by the
 * constructor, the least recently used entry is removed. The *used* term means
 * that is was either added or or looked up.
 *
 * Internally the cache uses std::unordered_map to hold the values for keys
 * and \ref lru_order_list to mantain the order of recent uses.
 */
template <class Key, class Value> class lru {

  lru_order_list<Key> order;
  std::unordered_map<Key,
                     std::pair<Value, typename decltype(order)::node_marker>>
      cache;
  size_t max_size;

public:
  /** Creates the cache of given size */
  lru(size_t max_size) : max_size(max_size) {
    if (max_size < 2) {
      throw std::invalid_argument("Max size must be at least 2");
    }
  }

  /** Asks for element from the cache under the given key and updates its
   * recently used status. \param[key] The key to look for
   *
   * \returns an empty optional if the element does not exist in the cache or a
   * reference to the value if the key exists. The reference is valid until the
   * next element is added to the cache.
   */
  std::optional<std::reference_wrapper<Value>> lookup(const Key &key) {
    auto it = cache.find(key);
    if (it != cache.end()) {
      order.mark_most_recently_used(it->second.second);
      return it->second.first;
    } else {
      return std::optional<std::reference_wrapper<Value>>();
    }
  }

  /** Adds the element to the cache at the given key.
   *  \param key The key to put the element at
   *  \param value The value to put in the cache
   *
   *  \return A pair of bool -- true if the new key was added, false if the
   * value for the key was replaced, and the reference to the value currently
   * stored in the cache. The reference is valid until the next element is added
   * to the cache.
   *
   *  Adds the new element to the cache if the key is not present in the cache.
   * If the key is present the value for the key is replaced and the recently
   * used status is updated.
   */
  template <typename T>
  std::pair<bool, Value &> put(const Key &key, T &&value) {
    auto existing = cache.find(key);
    if (existing != cache.end()) {
      existing->second.first = std::forward<T>(value);
      order.mark_most_recently_used(existing->second.second);
      return std::pair<bool, Value &>(false, existing->second.first);
    } else {
      if (cache.size() == max_size) {
        auto key_to_remove = order.pop_least_recently_used();
        assert(key_to_remove.has_value());
        [[maybe_unused]] auto ret = cache.erase(key_to_remove.value());
        assert(ret == true);
      }
      auto node = order.push_most_recently_used(key);
      auto insert_result = cache.insert(
          std::make_pair(key, std::make_pair(std::forward<T>(value), node)));
      assert(insert_result.second == true);
      return std::pair<bool, Value &>(true, insert_result.first->second.first);
    }
  }

  /** Adds the value if it is not in the cache, returns the existing value
   * otherwise.
   *
   * \param key The key to add to the cache.
   * \param func  The
   * function that will return the element to add if the addition is needed.
   *
   *  \returns a pair of bool (true if the value was added, false if the value
   * from the cache was used) and the reference to the value in the cache. The
   * reference is valid until the next element is added to the cache.
   */
  template <generator<Value> Func>
  std::pair<bool, Value &> lookup_or_put(const Key &key, Func &&func) {
    auto res = lookup(key);
    if (res) {
      return std::pair<bool, Value &>(false, res.value());
    } else {
      return put(key, func());
    }
  }
};

} // namespace jk::cpputil::lru

#endif /* LRU_H */
