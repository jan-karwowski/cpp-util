// SPDX-License-Identifier: GPL-3.0-only
#ifndef COMPUTATION_MANAGER_H
#define COMPUTATION_MANAGER_H

#include <cassert>
#include <concepts>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <map>
#include <mutex>
#include <optional>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "cpp-util/basic_concepts.hpp"

namespace jk::cpputil::computation_manager {

/** \brief Thrown when \ref
 * computation_manager::schedule_computation_for_category is called after the
 * computation manager's desctructor is started (note that due to blocking
 * nature of this descturctor it may execute for some time).
 *
 * Should not occur in properly written programs.
 */
struct termination_started_exception : std::exception {
  const char *what() const noexcept {
    return "Termination started. Cannot shedule new jobs";
  }
};

/** \brief Every task run by computation manager must implement this interface
 *
 * There is covenience implementation \ref cancellable_computation_from_function
 * provided which transforms a lamba accepting a \ref std::stop_token int a \ref
 * computation.
 */
struct computation {
  /** A main method running the computation. It will be run in another thread.
   */
  virtual void run() = 0;
  /** \brief Method run when \ref computation_manager requests cancelation of
   * this task
   *
   * This method is called by the \ref computation_manager when cancelation of
   * this task is requested and it is already running. The implementation MUST
   * assume that \ref cancel method will be called from another thread than \ref
   * run method. The implementation should terminate the \ref run as fast as
   * possible after this method is called.
   */
  virtual void cancel() = 0;
  virtual ~computation(){};
};

/** \brief A class adapting an \ref std::function taking one argument -- a stop
 * token into a \ref computation.
 *
 */
class cancellable_computation_from_function : public computation {
  std::function<void(std::stop_token st)> func;
  std::stop_source ssource;

public:
  /** \brief Construct a \ref computation
   *
   * @param func A function object that accepts one argument a \ref
   * std::stop_token. If the task is requested while running the token will be
   * used to notify the function about that fact. The function should respect
   * the cancelation request and terminate as soon as possible.
   */
  cancellable_computation_from_function(
      std::function<void(std::stop_token st)> &&func)
      : func(func) {}
  cancellable_computation_from_function(
      const std::function<void(std::stop_token st)> &func)
      : func(func) {}
  virtual void run();
  virtual void cancel();
  virtual ~cancellable_computation_from_function() {}
};

/** \brief Manages a fixed number of threads and a queue of tasks. Executes
 * tasks in FIFO order on threads.
 *
 * Each task is scheduled using \ref schedule_computation_for_categor with a
 * \ref Category paramteter. Category does not impact the order of scheduling,
 * the tasks are scheduled but is stored in a queue together with the task. The
 * scheduling function returns an unique task id. The id can be used for
 * cancelation of the task (scheduled or running).
 *
 * The categories. Besides canceling a single task it is possible to \ref
 * cancel_all_computations_for_category. This solves a use-case where the user
 * asks to generate several reports, for example for March, April and May. Each
 * report consists of 3 separate tasks. Everything is computed in parallel. At
 * some point for some reason the user decides to close Aplil's report before it
 * is computed. By putting each report's tasks to distinct category, e.g.
 * "march", "april", "may" you can cancel all the remaining tasks for the given
 * report.
 */
template <Hashable Category> class computation_manager {
  std::vector<std::jthread> threads;

  std::map<unsigned int, std::pair<Category, std::unique_ptr<computation>>>
      queue;
  std::unordered_map<unsigned int,
                     std::pair<Category, std::unique_ptr<computation>>>
      running;
  unsigned int highest_running_id;
  std::unordered_map<Category, std::unordered_set<unsigned int>>
      jobs_in_category;
  std::mutex queue_mutex; // the mutex procects a tuple of (queue,
                          // highest_running_it, jobs_for_category)
  std::condition_variable queue_wait;

  std::atomic<bool> termination_started;

  unsigned int counter;

private:
  void cancel_everything() {
    termination_started = true;
    std::unique_lock l(queue_mutex);
    for (const auto &entry : running) {
      entry.second.second->cancel();
    }
    queue.clear();
    queue_wait.notify_all();
    l.unlock();
  }

  std::optional<std::pair<unsigned int, computation *>> get_first_from_queue() {
    std::unique_lock l(queue_mutex);

    if (termination_started) {
      return std::optional<std::pair<unsigned int, computation *>>();
    }

    if (queue.empty()) {
      queue_wait.wait(l);
    }

    if (termination_started) {
      return std::optional<std::pair<unsigned int, computation *>>();
    }

    assert(!queue.empty());

    std::pair<unsigned int, computation *> ret;
    ret = std::make_pair(queue.begin()->first,
                         queue.begin()->second.second.get());
    highest_running_id = ret.first;
    running.insert(
        std::make_pair(queue.begin()->first, std::move(queue.begin()->second)));
    queue.erase(queue.begin());
    return ret;
  }

  // called from the other thread, must synchronize!
  void computation_finished(unsigned int id) {
    std::unique_lock l(queue_mutex);
    auto it = running.find(id);
    assert(it != running.end());
    Category category = it->second.first;
    running.erase(it);
    set_cleanup(id, category);
    l.unlock();
  }

  void set_cleanup(unsigned int id, const Category &category) {
    auto the_set = jobs_in_category.find(category);
    if (the_set->second.size() == 1) {
      jobs_in_category.erase(the_set);
    } else {
      the_set->second.erase(id);
    }
  }

public:
  /** \brief purges the queue, cancels all running computations and joins all
   * the threads
   *
   * Note: joining threads is a blocking operation.
   */
  ~computation_manager() {
    cancel_everything();
    for (auto &th : threads) {
      th.join();
    }
  }

  /** \brief Creates a new manager with an epty queue and \ref thread_count
   * threads.
   */
  computation_manager(unsigned int thread_count)
      : highest_running_id(0), termination_started(false), counter(1) {
    for (unsigned int i = 0; i < thread_count; i++) {
      threads.emplace_back([this]() {
        bool loop = true;
        while (loop) {
          auto to_process = get_first_from_queue();
          if (to_process) {
            auto &p = to_process.value();
            p.second->run();
            computation_finished(p.first);
          } else {
            loop = false;
          }
        }
      });
    }
  }

  /** \brief Removes all queued tasks for the category and calls \ref
   * computation::cancel for each task running in category. */
  void cancel_all_computations_for_category(const Category &category) {
    std::unique_lock l(queue_mutex);

    std::vector<unsigned int> to_remove;
    auto set = jobs_in_category.find(category);
    if (set != jobs_in_category.end()) {
      for (unsigned int job : set->second) {
        if (job <= highest_running_id) {
          running.find(job)->second.second->cancel();
        } else {
          queue.erase(job);
          to_remove.push_back(job);
        }
      }
      for (unsigned int job : to_remove) {
        set->second.erase(job);
      }
      if (set->second.empty()) {
        jobs_in_category.erase(set);
      }
    }
    l.unlock();
  }

  /** \brief if the computation with id is running, calls \ref
   * computation::cancel on it, if it is in the queue removes it from the queue.
   * If there is no task with this id (either the task was already finished or
   * no task with this id was ever scheduled it does nothing.
   *
   * \return true if a task was cancelled/removed from the queue, false
   * otehrwise
   */
  bool cancel_computation(unsigned int id) {
    std::unique_lock l(queue_mutex);

    auto it = queue.find(id);
    if (it == queue.end()) {
      auto running_it = running.find(id);
      if (running_it == running.end()) {
        return false;
      } else {
        it->second.second->cancel();
        return true;
      }
    } else {
      Category &s = it->second.first;
      set_cleanup(id, s);
      queue.erase(it);
      return true;
    }

    l.unlock();
  }

  /** \bried Queues a \ref computation with given category. The ownership of the
   * computation is transfered to the manager by \ref std::unique_pointer.
   *
   * \returns The id of the scheduled task that can be used with \ref
   * cancel_computation
   */
  unsigned int schedule_computation_for_category(
      const Category &category, std::unique_ptr<computation> &&computation) {
    if (termination_started) {
      throw new termination_started_exception();
    }
    unsigned int number = counter;
    counter++;
    std::unique_lock l(queue_mutex);
    auto it = jobs_in_category.find(category);
    if (it == jobs_in_category.end()) {
      it = jobs_in_category
               .insert(
                   std::make_pair(category, std::unordered_set<unsigned int>()))
               .first;
    }
    it->second.insert(number);
    queue.insert(std::make_pair(
        number, std::make_pair(category, std::move(computation))));
    l.unlock();
    queue_wait.notify_one();
    return number;
  }
};
} // namespace jk::cpputil::computation_manager

#endif /* COMPUTATION_MANAGER_H */
