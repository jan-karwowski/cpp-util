cmake_minimum_required(VERSION 3.18)

option(BUILD_DOXYGEN "Build doxygen documentation" OFF)
option(CPP_UTIL_DISABLE_BOOST OFF)


set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

project(cpp-util LANGUAGES CXX)


if( CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR )
  set(BUILD_TESTS ON)
else()
  set(BUILD_TESTS OFF)
endif()


if(BUILD_TESTS)
  include(CTest)
  add_subdirectory(test)
endif()

if(NOT CPP_UTIL_DISABLE_BOOST)
  add_subdirectory(boost-util)
  if(BUILD_TESTS)
    add_subdirectory(boost-util-test)
  endif()
endif()

add_library(cpp-util STATIC src/computation_manager.cpp)
target_compile_features(cpp-util PUBLIC cxx_std_20)
target_include_directories(cpp-util PUBLIC include)

find_package(Doxygen)

if(DOXYGEN_FOUND AND BUILD_DOXYGEN)
  doxygen_add_docs(doc include)
endif(DOXYGEN_FOUND AND BUILD_DOXYGEN)
