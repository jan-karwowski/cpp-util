// SPDX-License-Identifier: GPL-3.0-only

#define BOOST_TEST_MODULE boost_helpers_test
#include <boost/test/unit_test.hpp>

#include "hana_string_helpers.hpp"

using namespace jk::cpputil::boost_helpers::hana_string;

BOOST_AUTO_TEST_SUITE(string_hana_helpers_test)

static_assert(to_hana_string_v<0> == BOOST_HANA_STRING("0"));
static_assert(to_hana_string_v<1> == BOOST_HANA_STRING("1"));
static_assert(to_hana_string_v<2> == BOOST_HANA_STRING("2"));
static_assert(to_hana_string_v<10> == BOOST_HANA_STRING("10"));
static_assert(to_hana_string_v<12345> == BOOST_HANA_STRING("12345"));

BOOST_AUTO_TEST_SUITE_END()
