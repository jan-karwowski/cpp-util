set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


find_package(Boost COMPONENTS unit_test_framework REQUIRED)
add_executable(test_boost test_hana_helpers.cpp)
target_link_libraries(test_boost PRIVATE boost-util Boost::unit_test_framework)
target_compile_options(test_boost PRIVATE -Wall -Wextra -fsanitize=address,undefined)
target_link_options(test_boost PRIVATE -fsanitize=address,undefined)
add_test(NAME test_boost_lib COMMAND test_boost)
