#ifndef HANA_STRING_HELPERS_H
#define HANA_STRING_HELPERS_H

#include <boost/hana.hpp>

namespace jk::cpputil::boost_helpers::hana_string {

template <unsigned int i> class to_hana_string {
  template <unsigned int val, char... chars>
  struct to_character_pack
      : to_character_pack<val / 10, static_cast<char>(val % 10) + '0',
                          chars...> {};
  template <char... chars> struct to_character_pack<0, chars...> {
    constexpr static auto hana_str = boost::hana::string_c<chars...>;
  };
  template <> struct to_character_pack<0> {
    constexpr static auto hana_str = BOOST_HANA_STRING("0");
  };

public:
  static constexpr auto str = to_character_pack<i>::hana_str;
};

template <unsigned int i>
inline constexpr auto to_hana_string_v = to_hana_string<i>::str;

} // namespace jk::cpputil::boost_helpers::hana_string

#endif /* HANA_STRING_HELPERS_H */
